package com.ljbv.amoba.dataBase

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ljbv.amoba.AuthToken

@Dao
interface Dao {


    @Query("SELECT * FROM Sesion")
    fun sesionroom() : Sesion

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertToken(AuthToken: Token)

    @Query("SELECT * FROM Token")
    fun getTokenRoom(): Token

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSesion(sesion: Sesion)

    @Query("DELETE FROM Token")
    suspend fun delete()
}