package com.ljbv.amoba.dataBase

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class PacienteRoom(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    val paciente : String?,
    val correo: String?,
    val ci: String?,
    val sexo:String?,
    val direction:String?,
    val phone:String?,
    val mobile:String?,
    val edad:String?,
    val photo:String?,
)

@Entity
data class Token(
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0,
    val token_type: String,
    val expires_in:Int,
    val access_token:String,
    val refresh_token:String
)

@Entity
data class Sesion(
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0,
    var sesionActiva: Boolean
)