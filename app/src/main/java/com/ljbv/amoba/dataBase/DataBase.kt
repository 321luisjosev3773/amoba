package com.ljbv.amoba.dataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [PacienteRoom::class,Token::class, Sesion::class], version = 1)
abstract class DataBase: RoomDatabase() {
        abstract fun Dao(): Dao

    companion object {
        private var INSTANCE: DataBase? = null
        fun getDatabase(context: Context?): DataBase {
            INSTANCE = INSTANCE ?: Room.databaseBuilder(
                context!!.applicationContext,
                DataBase::class.java,
                "AMOBA"
            ).build()
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }

        fun clearTables(context: Context) {
            getDatabase(context).clearAllTables()
        }
    }
    }
