package com.ljbv.amoba.ViewModel

import androidx.lifecycle.*
import androidx.lifecycle.ViewModel
import com.ljbv.amoba.AuthToken
import com.ljbv.amoba.dataBase.Sesion
import com.ljbv.amoba.dataBase.Token
import kotlinx.coroutines.launch
import java.lang.Exception

class ViewModel(private val repo: Repo): ViewModel() {

    fun delete(){
        viewModelScope.launch {
            repo.deleteRepo()
        }
    }
    fun setSesion(sesion: Sesion){
        viewModelScope.launch {
            repo.setSesionrepo(sesion)
        }
    }
    fun getSesion():Boolean{

     return try{
          repo.getSesionRepo()
      }catch (e: Exception){
          false
      }
    }

    fun setToken(token: Token){
         viewModelScope.launch {
             repo.setTokenRepo(token)
         }
    }
    fun getToken():Token{
        return repo.appDataBase.Dao().getTokenRoom()
    }
    
    

}


class VMFactory(private val repo: Repo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(Repo::class.java).newInstance(repo)
    }
}
