package com.ljbv.amoba.ViewModel

import androidx.lifecycle.LiveData
import com.ljbv.amoba.AuthToken
import com.ljbv.amoba.dataBase.DataBase
import com.ljbv.amoba.dataBase.Sesion
import com.ljbv.amoba.dataBase.Token

class Repo(dataBase: DataBase) {
    var appDataBase = dataBase
  suspend  fun deleteRepo(){
        appDataBase.Dao().delete()
    }
   fun getSesionRepo(): Boolean {
       return appDataBase.Dao().sesionroom().sesionActiva
   }

  suspend fun setTokenRepo(token: Token){
        appDataBase.Dao().insertToken(token)
    }
    suspend fun setSesionrepo(sesion: Sesion){
        appDataBase.Dao().insertSesion(sesion)
    }
}