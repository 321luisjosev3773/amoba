package com.ljbv.amoba

import android.os.Parcel
import android.os.Parcelable
import androidx.versionedparcelable.ParcelField
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Patients(
    val id : Int?,
    val history_id: Int?,
    val person_id: Int?,
    val doctor_id:Int?,
    val company_id:Int?,
    val history_order:Int?,
    val referred:String?,
    val referred_type:String?,
    val created_at:String?,
    val updated_at:String?,
    val deleted_at:String?,
    var person: Details?,
    var history: HistoryDetails?
):Parcelable

@Parcelize
data class Details(
    val id:Int?,
    val region_id:String?,
    val people_first_name:String?,
    val people_surnames:String?,
    val people_document:String?,
    val people_phone:String?,
    val people_mobile:String?,
    val email:String?,
    val people_birthday:String?,
    val people_sex:String?,
    val ima_profile_person:String?,
    val people_address:String?,
    val occupation:String?,
    val civil_status:String?,
    val Insurance:String?,
    val created_at:String?,
    val updated_at:String?,
    val deleted_at:String?
):Parcelable

@Parcelize
data class HistoryDetails(
    val id: Int?,
    val person_id: Int?,
    var history_data :HistoryData?,
    val fisical_exam:String?,
    val created_at:String?,
    val updated_at:String?,
    val deleted_at:String?,
    val diagnostic:ArrayList<String>?
):Parcelable

@Parcelize
data class HistoryData(
    val fisical_exam:String?,
    val family_background:String?,
    val personal_background:String?,
    val reason_consultation:String?
):Parcelable


data class AuthToken(
    val token_type: String,
    val expires_in:Int,
    val access_token:String,
    val refresh_token:String
)

data class AuthTokenNoPasswor(
    val token_type: String,
    val expires_in:Int,
    val access_token:String
)








