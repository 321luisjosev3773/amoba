package com.ljbv.amoba.services

import com.ljbv.amoba.AuthToken
import com.ljbv.amoba.Patients
import retrofit2.Call
import retrofit2.http.*


interface ApiServices {
    @POST("oauth/token")
    fun login( @Body body: Map<String, String?>): Call<AuthToken>

    @POST("api/v1/patient/all")
    fun list( @HeaderMap headers: Map<String, String>, @Body lista: Map<String, Int?>): Call<List<Patients>>

    @GET("api/v1/patient/search/{id}/1")
    fun person(@HeaderMap Authorization: Map<String, String>,@Path("id") id: Int ): Call<Patients>


}