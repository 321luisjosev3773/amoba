package com.ljbv.amoba.UI

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.navigation.findNavController
import com.ljbv.amoba.R
import com.ljbv.amoba.ViewModel.Repo
import com.ljbv.amoba.ViewModel.VMFactory
import com.ljbv.amoba.ViewModel.ViewModel
import com.ljbv.amoba.dataBase.DataBase
import com.ljbv.amoba.databinding.ActivitySplashBinding
import kotlinx.coroutines.*

class Splash : AppCompatActivity() {
    private var _binding: ActivitySplashBinding? = null
    private val binding get() = _binding!!
    private val SPLASH_SCREEN_PLAY: Long = 1000
    val job = Job()
    val errorHandler = CoroutineExceptionHandler { _, exception ->
        Log.e("LOG", "ERROR: $exception")
    }
    private val viewModel by viewModels<ViewModel> { VMFactory(Repo(DataBase.getDatabase(this.applicationContext))) }
     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
         CoroutineScope(Dispatchers.IO).launch {
             val sesion = viewModel.getSesion()
             splashLogica(sesion)
             Log.e("TAG", "Sesion ----------->: $sesion" )
         }


    }

    private fun splashLogica(sesion: Boolean) {
        val coroutineScope = CoroutineScope(job + Dispatchers.IO)
        coroutineScope.launch(errorHandler) {
            delay(SPLASH_SCREEN_PLAY)
                    val bundle = Bundle()
                    bundle.putBoolean("sesion", sesion)
                    val intent  = Intent().setClass(this@Splash, MainActivity::class.java)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finish()
        }
    }
}