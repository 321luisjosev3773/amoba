package com.ljbv.amoba.UI

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.ljbv.amoba.Patients
import com.ljbv.amoba.R
import com.ljbv.amoba.databinding.FragmentDialogDetallesBinding
import com.ljbv.amoba.databinding.FragmentLoginBinding
import com.ljbv.amoba.years

class DialogDetalles : DialogFragment() {
    lateinit var patient: Patients
    private var _binding: FragmentDialogDetallesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDialogDetallesBinding.inflate(inflater, container, false)
        val window: Window? = dialog?.window
        window?.setBackgroundDrawableResource(R.drawable.background_dialog)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireArguments().let {
            patient = it.getParcelable("entidad")!!
        }
        binding.iconExit.setOnClickListener {
            dismiss()
        }

        binding.TextNameInfoDetalle.text = patient.person!!.people_first_name.toString()
        binding.DNI.text = patient.person!!.people_document.toString()
        binding.Correo.text = patient.person!!.email.toString()
        binding.years.text = years(patient.person!!.people_birthday.toString())
        Glide.with(binding.IconPhoto).load(patient.person!!.ima_profile_person).into(binding.IconPhoto)
        when(patient.person!!.people_sex){
            "M" -> {
                binding.Sexo.text = "Masculino"
            }
            "F" -> {
                binding.Sexo.text = "Femenina"
            }
        }

        binding.Direction.text = patient.person!!.people_address.toString()
        binding.numberPhone.text = patient.person!!.people_phone.toString()
        binding.numberMobile.text = patient.person!!.people_mobile.toString()





    }

}