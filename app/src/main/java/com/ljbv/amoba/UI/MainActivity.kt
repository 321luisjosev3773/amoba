package com.ljbv.amoba.UI

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.ljbv.amoba.R
import com.ljbv.amoba.ViewModel.Repo
import com.ljbv.amoba.ViewModel.VMFactory
import com.ljbv.amoba.ViewModel.ViewModel
import com.ljbv.amoba.dataBase.DataBase
import com.ljbv.amoba.dataBase.Sesion
import com.ljbv.amoba.databinding.ActivityMainBinding
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main

class MainActivity : AppCompatActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel by viewModels<ViewModel> { VMFactory(Repo(DataBase.getDatabase(this.applicationContext))) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        var parametros: Boolean = intent.extras!!.getBoolean("sesion")

        if (parametros != null){
            when(parametros) {
                true -> { findNavController(R.id.nav_host_fragment).navigate(R.id.listPerson) }
                else -> { findNavController(R.id.nav_host_fragment).navigate(R.id.login) }
            }
        }


//        viewModel.getSesion{
//            Log.e("TAG", "Sesion ----------->: $it" )
//            when(it) {
//                true -> { findNavController(R.id.nav_host_fragment).navigate(R.id.listPerson) }
//                else -> { findNavController(R.id.nav_host_fragment).navigate(R.id.login) }
//            }
//        }
//        CoroutineScope(IO).launch {
//            val sesion = viewModel.getSesion()
//                Log.e("TAG", "Sesion ----------->: $sesion" )
//            withContext(Main){
//                when(sesion) {
//                        true -> { findNavController(R.id.nav_host_fragment).navigate(R.id.listPerson) }
//                        else -> { findNavController(R.id.nav_host_fragment).navigate(R.id.login) }
//                    }
//            }
//        }
    }

}