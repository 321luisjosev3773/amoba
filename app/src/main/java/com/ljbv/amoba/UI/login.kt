package com.ljbv.amoba.UI

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieAnimationView
import com.ljbv.amoba.AuthToken
import com.ljbv.amoba.R
import com.ljbv.amoba.ViewModel.Repo
import com.ljbv.amoba.ViewModel.VMFactory
import com.ljbv.amoba.ViewModel.ViewModel
import com.ljbv.amoba.dataBase.DataBase
import com.ljbv.amoba.dataBase.Sesion
import com.ljbv.amoba.dataBase.Token
import com.ljbv.amoba.databinding.FragmentLoginBinding
import com.ljbv.amoba.databinding.ProgressDialogLayoutBinding
import com.ljbv.amoba.getParth
import com.ljbv.amoba.services.Retrofit
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import org.json.JSONObject
import retrofit2.awaitResponse

class login : Fragment() {
    private var passwords = ""
    private var emails= ""
    private lateinit var progress : ProgressDialogLayoutBinding
    private lateinit var progressBar: ProgressDialog
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var api: Retrofit
    private val viewModel by activityViewModels<ViewModel> { VMFactory(Repo(DataBase.getDatabase(requireContext().applicationContext))) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        api = Retrofit
        progress = ProgressDialogLayoutBinding.inflate(LayoutInflater.from(context))
        progressBar = ProgressDialog(requireContext())

        binding.botonLogin.setOnClickListener {
           login()

        }
    }

    private fun login() {
        val result = arrayOf(validateEmail(),validatePassword())
        if(false in result){
            return
        }else{

            signin(emails, passwords)
        }
    }

    private fun signin(email: String, password: String) {
        progressBar = ProgressDialog(context)
        progressBar.setCanceledOnTouchOutside(false)
        progressBar.setCancelable(false)
        progressBar.show()
        progressBar.setContentView(R.layout.progress_dialog_layout)
        progressBar.findViewById<TextView>(R.id.tvMessage).text = "Cargando..."
        progressBar.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        val job = Job()
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progressBar.dismiss()
            Toast.makeText(requireContext(),"Error de conxión" , Toast.LENGTH_SHORT).show()
            Log.e("LOG", "ERROR login: $exception")

        }

        val coroutineScope = CoroutineScope(job + Main)
        coroutineScope.launch(errorHandler) {

            val response = api.service.login(
                getParth(email,password)

            ).awaitResponse()


            if(response.isSuccessful){
                progressBar.dismiss()
                withContext(Main){

                    viewModel.setSesion(Sesion(0, true))
                    viewModel.setToken(Token(0,response.body()!!.token_type,
                        response.body()!!.expires_in,
                        response.body()!!.access_token,
                        response.body()!!.refresh_token
                    ))
                    findNavController().navigate(R.id.action_login_to_listPerson)
                }
            }else{
                progressBar.dismiss()
                Toast.makeText(requireContext(), "Datos Incorrectos", Toast.LENGTH_SHORT).show()
            }
        }

    }



    private fun validatePassword(): Boolean {
        val password= binding.passwordLogin.editText?.text.toString()

        return if(password.isEmpty()){
            binding.passwordLogin.error = "La contraseña esta vacío"
            binding.LCRR.setBackgroundResource(R.drawable.round_border_red)
            false
        }else{
            passwords = password
            binding.passwordLogin.error = null
            binding.LCRR.setBackgroundResource(R.drawable.border)
            true
        }
    }

    private fun validateEmail(): Boolean {
        val email= binding.emailLogin.editText?.text.toString()

        return if(email.isEmpty()){
            binding.emailLogin.error = "El correo está vacío"
            binding.LCR.setBackgroundResource(R.drawable.round_border_red)
            false
        }else{
            binding.LCR.setBackgroundResource(R.drawable.border)
            binding.emailLogin.error = null
            emails = email
            true
        }
    }


}
