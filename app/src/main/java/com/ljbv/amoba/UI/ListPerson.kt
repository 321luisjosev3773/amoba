package com.ljbv.amoba.UI

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.ljbv.amoba.*
import com.ljbv.amoba.Utils.Onclick
import com.ljbv.amoba.ViewModel.Repo
import com.ljbv.amoba.ViewModel.VMFactory
import com.ljbv.amoba.ViewModel.ViewModel
import com.ljbv.amoba.dataBase.DataBase
import com.ljbv.amoba.dataBase.Sesion
import com.ljbv.amoba.dataBase.Token
import com.ljbv.amoba.databinding.FragmentListPersonBinding
import com.ljbv.amoba.databinding.ProgressDialogLayoutBinding
import com.ljbv.amoba.services.Retrofit
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import retrofit2.awaitResponse


class ListPerson : Fragment(), Onclick {
    private var _binding: FragmentListPersonBinding? = null
    private val binding get() = _binding!!
    private lateinit var api: Retrofit
    private lateinit var progress : ProgressDialogLayoutBinding
    private lateinit var progressBar: ProgressDialog
    var token : String? = ""
    var tipo=""
    private val viewModel by activityViewModels<ViewModel> { VMFactory(Repo(DataBase.getDatabase(requireContext().applicationContext))) }

    lateinit var layoutManager: LinearLayoutManager
    private var list = ArrayList<Patients>()
    private val adapter: AdapterListPerson by lazy {
        AdapterListPerson(
            requireContext(),
            arrayListOf(),
            this
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListPersonBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        api = Retrofit
        progress = ProgressDialogLayoutBinding.inflate(LayoutInflater.from(context))
        progressBar = ProgressDialog(requireContext())

        query()

        binding.Refresh.setOnClickListener {
            query()
        }
        binding.exitApp.setOnClickListener {
            exitApp()
        }
        CoroutineScope(IO).launch {
            viewModel.getToken().let {
                token  = it.access_token
                tipo= it.token_type

            }
        }

    }

    private fun exitApp() {
        viewModel.delete()
        viewModel.setSesion(Sesion(0,false))
        requireActivity().finish()
        startActivity(requireActivity().intent)
    }

    private fun query() {
        binding.Recycler.visibility = View.VISIBLE
        progressBar = ProgressDialog(context)
        progressBar.setCanceledOnTouchOutside(false)
        progressBar.setCancelable(false)
        progressBar.show()
        progressBar.setContentView(R.layout.progress_dialog_layout)
        progressBar.findViewById<TextView>(R.id.tvMessage).text = "Cargando..."
        progressBar.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        val job = Job()
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progressBar.dismiss()
            binding.Recycler.visibility = View.INVISIBLE
            Toast.makeText(requireContext(), "Error de conexión", Toast.LENGTH_SHORT).show()
            Log.e("LOG", "ERROR Personas: $exception")

        }

        val coroutineScope = CoroutineScope(job + Dispatchers.Main)
        coroutineScope.launch(errorHandler) {

            val response = api.service.list(
                header("l", token!!, tipo),
                getParth_auth()
            ).awaitResponse()

            if(response.isSuccessful){
                withContext(Dispatchers.Main){
                    progressBar.dismiss()
                    list.addAll(response.body()!! as ArrayList<Patients>)
                    layoutManager = LinearLayoutManager(requireContext())
                    binding.Recycler.layoutManager = layoutManager
                    binding.Recycler.adapter = adapter
                    adapter.addRowItems(list)

                }
            }else{
                binding.Recycler.visibility = View.INVISIBLE
                progressBar.dismiss()
                Toast.makeText(requireContext(), "Fallo de conexión", Toast.LENGTH_SHORT).show()
            }
        }
    }



    override fun onClick(patient: Patients) {
        val dialog = DialogDetalles()
        val bundle = Bundle()
        bundle.putParcelable("entidad", patient)
        dialog.arguments = bundle
        dialog.show(requireActivity().supportFragmentManager, "")
    }

}