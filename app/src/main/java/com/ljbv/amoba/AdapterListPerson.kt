package com.ljbv.amoba

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ljbv.amoba.Utils.Onclick
import com.ljbv.amoba.databinding.ListUserBinding

class AdapterListPerson(private val context: Context,
                        private val listPeople: ArrayList<Patients>,
                        private var listener : Onclick,):
    RecyclerView.Adapter<AdapterListPerson.ListPersonViewHolder>() {

    private var img = ""
    private var mLastClickTime = System.currentTimeMillis()
    private val CLICK_TIME_INTERVAL: Long = 300

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListPersonViewHolder {
        val view = ListUserBinding.inflate(LayoutInflater.from(context) , parent,false)
        return ListPersonViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListPersonViewHolder, position: Int) {
        val item = listPeople[position]
        holder.setListener(item)
        Glide.with(holder.viewBinding.IconPhoto).load(item.person!!.ima_profile_person).into(holder.viewBinding.IconPhoto)
        holder.viewBinding.TextNameInfo.text = item.person!!.people_first_name.toString()


    }
    inner class ListPersonViewHolder(var viewBinding: ListUserBinding) : RecyclerView.ViewHolder(viewBinding.root) {
        fun setListener(paciente: Patients) {
            with(viewBinding.root) {
                setOnClickListener {
                    val now = System.currentTimeMillis()
                    when (now - mLastClickTime < CLICK_TIME_INTERVAL) {
                        true -> {
                            return@setOnClickListener
                        }
                        else -> {
                            mLastClickTime = now
                            listener.onClick(paciente)
                        }
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return listPeople.size
    }

    fun addRowItems(rowItems: ArrayList<Patients>) {
        this.listPeople.addAll(rowItems)
    }
}