package com.ljbv.amoba

import android.content.Context
import com.ljbv.amoba.dataBase.DataBase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

fun createTables(context: Context){
    val room = DataBase.getDatabase(context)
    //----------ROOM CREACION DE TABLES
    room.Dao()
}

fun header(s: String, token: String, tipo: String): Map<String, String> {
    return when(s){
        "p" -> {
            mapOf(
                Pair("Authorization", "$tipo $token")
            )
        }
        "l" -> {
            mapOf(
                Pair("Authorization", "$tipo $token"),
                Pair("Content-Type", "application/json")
            )
        }
        else ->{
            mapOf(
                Pair("Authorization", "$tipo $token")
            )
        }
    }
}
fun getParth_auth(): Map<String, Int> {
    return  mapOf(
        Pair("doctor_id", 1),
        Pair("company_id", 1)
    )

}

fun getParth(email: String, password: String): Map<String, String> {
    return  mapOf(
        Pair("grant_type", "password"),
        Pair("client_id", "2"),
        Pair("client_secret", "rDnLA1OfY8nHovztVBU8rcvOP9K5iH7LngUZNIzB"),
        Pair("username", email),
        Pair("password", password)

    )

}
fun years(year:String): String{
    var fechaNac: Date? = null
    fechaNac = SimpleDateFormat("yyyy-MM-dd").parse(year)

    val fechaNacimiento: Calendar = Calendar.getInstance()
    //Se crea un objeto con la fecha actual
    val fechaActual: Calendar = Calendar.getInstance()
    //Se asigna la fecha recibida a la fecha de nacimiento.
    fechaNacimiento.time = fechaNac
    //Se restan la fecha actual y la fecha de nacimiento
    var año: Int = fechaActual.get(Calendar.YEAR) - fechaNacimiento.get(Calendar.YEAR)
    val mes: Int = fechaActual.get(Calendar.MONTH) - fechaNacimiento.get(Calendar.MONTH)
    val dia: Int = fechaActual.get(Calendar.DATE) - fechaNacimiento.get(Calendar.DATE)
    //Se ajusta el año dependiendo el mes y el día
    if (mes < 0 || mes == 0 && dia < 0) {
        año--
    }

    return "$año Años"
}
